package com.zscorpio.pkhelper.helpers;

import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.scene.Pickables;

import com.zscorpio.pkhelper.PKHelper;
import org.rspeer.ui.Log;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Paint
{
    ///////////////////////////////////////////////////////////////////

    private Graphics graphics = null;
    private String healthString = "";
    private int healthStringX = 0;
    private String strengthBoostString = "";
    private int strengthBoostStringX = 0;
    private String ammoInQuiverString = "";
    private int ammoInQuiverStringX = 0;
    private int ammoInQuiver = 0;
    private String maxComboOutputString = "";
    private int targetNameX = 0;
    private String myMaxComboOutputString = "";
    private int myMaxComboOutputStringX = 0;

    ///////////////////////////////////////////////////////////////////

    private final PKHelper script;
    private BufferedImage paintBase;
    private BufferedImage sidePanelNoTarget;
    private BufferedImage sidePanelTarget;
    private BufferedImage sidePanelDisabled;
    private BufferedImage redValueBar;
    private BufferedImage qIconActive;
    private BufferedImage wIconActive;
    private BufferedImage eIconActive;
    private BufferedImage rIconActive;
    private BufferedImage fIconActive;
    private BufferedImage qIconButtonActive;
    private BufferedImage wIconButtonActive;
    private BufferedImage eIconButtonActive;
    private BufferedImage rIconButtonActive;
    private BufferedImage fIconButtonActive;
    private final BufferedImage[] sidepanelCombo = {null, null, null, null};

    ///////////////////////////////////////////////////////////////////

    private static final Font FONT_ARIAL = new Font("Arial", Font.PLAIN, 11);
    private static final Color COLOR_BLACK = new Color(0, 0, 0, 255);
    private static final Color COLOR_SKYBLUE = new Color(66, 176, 244, 100);
    private static final Color COLOR_RED = new Color(255, 0, 0, 100);
    private static final Color COLOR_RED_SCORPIO = new Color(192, 16, 16, 255);
    private static final Color COLOR_GREEN = new Color(0, 255, 0, 100);
    private static final Color COLOR_WHITE_TRANSPARENT = new Color(255, 255, 255, 100);

    ///////////////////////////////////////////////////////////////////

    public Paint(PKHelper script)
    {
        this.script = script;
        PKHelper.getExecutor().execute(() ->
        {
            try
            {
                paintBase = ImageIO.read(new URL("https://i.imgur.com/orhWbKM.png"));
                Thread.sleep(200);
                sidePanelNoTarget = ImageIO.read(new URL("http://i64.tinypic.com/2e5nwgy.png"));
                Thread.sleep(200);
                sidePanelTarget = ImageIO.read(new URL("http://oi64.tinypic.com/2u9olyw.jpg"));
                Thread.sleep(200);
                sidePanelDisabled = ImageIO.read(new URL("http://i67.tinypic.com/21e6j3r.png"));
                Thread.sleep(200);
                redValueBar = ImageIO.read(new URL("https://i.imgur.com/ByHJ7HY.png"));
                Thread.sleep(200);
                qIconActive = ImageIO.read(new URL("https://i.imgur.com/0GfZ0xc.png"));
                Thread.sleep(200);
                wIconActive = ImageIO.read(new URL("https://i.imgur.com/UUCyJGU.png"));
                Thread.sleep(200);
                eIconActive = ImageIO.read(new URL("https://i.imgur.com/rvIrEOs.png"));
                Thread.sleep(200);
                rIconActive = ImageIO.read(new URL("https://i.imgur.com/2wgjLtG.png"));
                Thread.sleep(200);
                fIconActive = ImageIO.read(new URL("https://i.imgur.com/T2ZhJt4.png"));
                Thread.sleep(200);
                qIconButtonActive = ImageIO.read(new URL("https://i.imgur.com/rOBAUlD.png"));
                Thread.sleep(200);
                wIconButtonActive = ImageIO.read(new URL("https://i.imgur.com/C1RFK7x.png"));
                Thread.sleep(200);
                eIconButtonActive = ImageIO.read(new URL("https://i.imgur.com/Ycj2HFG.png"));
                Thread.sleep(200);
                rIconButtonActive = ImageIO.read(new URL("https://i.imgur.com/zqarrF3.png"));
                Thread.sleep(200);
                fIconButtonActive = ImageIO.read(new URL("https://i.imgur.com/x15JOu8.png"));
                Thread.sleep(200);
                sidepanelCombo[0] = ImageIO.read(new URL("https://i.imgur.com/qfp17OT.png"));
                Thread.sleep(200);
                sidepanelCombo[1] = ImageIO.read(new URL("https://i.imgur.com/OK8GeEF.png"));
                Thread.sleep(200);
                sidepanelCombo[2] = ImageIO.read(new URL("https://i.imgur.com/IxdGTAi.png"));
                Thread.sleep(200);
                sidepanelCombo[3] = ImageIO.read(new URL("https://i.imgur.com/07B43J9.png"));
                Thread.sleep(200);
            }
            catch (InterruptedException e)
            {
            }
            catch (MalformedURLException e)
            {
                Log.severe(e);
            }
            catch (IOException e)
            {
                Log.severe(e);
            }
        });
    }

    ///////////////////////////////////////////////////////////////////

    public void draw(Graphics g, PKHelper script)
    {
        try
        {
            if (!script.getKeyListener().getToggle())
            {
                if (sidePanelDisabled != null)
                    g.drawImage(sidePanelDisabled, 0, 339, null);
            }
            else
            {
                ///////////////////////////////////////////////////////

                g.setFont(FONT_ARIAL);
                if (graphics == null)
                    graphics = g;

                ///////////////////////////////////////////////////////

                g.setColor(COLOR_WHITE_TRANSPARENT);
                for (Pickable item : script.getAvailablePickables())
                {
                    if (item.getPosition().toScreen() != null)
                        g.drawString(item.getStackSize() + " arrow", item.getPosition().toScreen().getX(), item.getPosition().toScreen().getY());
                }

                ///////////////////////////////////////////////////////

                if (script.getTarget() == null)
                {
                    g.setColor(COLOR_GREEN);
                    for (Player possibleTarget : script.getPossibleTargets())
                    {
                        possibleTarget.getPosition().outline(g);
                    }
                }

                ///////////////////////////////////////////////////////

                if (script.getBitch() != null)
                {
                    g.setColor(COLOR_RED);
                    script.getBitch().getPosition().outline(g);
                }

                ///////////////////////////////////////////////////////

                if (script.getTarget() != null)
                {
                    g.setColor(COLOR_SKYBLUE);
                    script.getTarget().getPosition().outline(g);
                }

                ///////////////////////////////////////////////////////

                g.drawImage(paintBase, 0, 339, null);

                if (script.getTarget() == null)
                {
                    g.drawImage(sidePanelNoTarget, 0, 339, null);
                }
                else
                {
                    g.drawImage(sidePanelTarget, 0, 339, null);
                }

                ///////////////////////////////////////////////////////

                if (script.getKeyListener().getPerformingKey() > -1)
                {
                    if (!script.getKeyListener().isPerformingKeyReleased())
                    {
                        switch (script.getKeyListener().getPerformingKey())
                        {
                            case KeyListener.KEY_KO_2H:
                            {
                                g.drawImage(qIconButtonActive, 155, 449, null);
                                break;
                            }
                            case KeyListener.KEY_RANGED:
                            {
                                g.drawImage(wIconButtonActive, 217, 449, null);
                                break;
                            }
                            case KeyListener.KEY_EAT:
                            {
                                g.drawImage(eIconButtonActive, 279, 449, null);
                                break;
                            }
                            case KeyListener.KEY_DRINK_POTION:
                            {
                                g.drawImage(rIconButtonActive, 341, 449, null);
                                break;
                            }
                            case KeyListener.KEY_LOOT_ARROWS:
                            {
                                g.drawImage(fIconButtonActive, 465, 449, null);
                                break;
                            }
                            default:
                            {

                            }
                        }
                    }

                    ///////////////////////////////////////////////////////

                    if (script.getKeyListener().isPerforming())
                    {
                        switch (script.getKeyListener().getPerformingKey())
                        {
                            case KeyListener.KEY_KO_2H:
                            {
                                g.drawImage(qIconActive, 148, 393, null);
                                break;
                            }
                            case KeyListener.KEY_RANGED:
                            {
                                g.drawImage(wIconActive, 210, 393, null);
                                break;
                            }
                            case KeyListener.KEY_EAT:
                            {
                                g.drawImage(eIconActive, 272, 393, null);
                                break;
                            }
                            case KeyListener.KEY_DRINK_POTION:
                            {
                                g.drawImage(rIconActive, 334, 393, null);
                                break;
                            }
                            case KeyListener.KEY_LOOT_ARROWS:
                            {
                                g.drawImage(fIconActive, 458, 393, null);
                                break;
                            }
                            default:
                            {

                            }
                        }
                    }
                }

                ///////////////////////////////////////////////////////

                if (script.getTarget() != null && targetNameX != 0)
                {
                    g.setColor(COLOR_RED_SCORPIO);

                    g.drawString(script.getTarget().getNamePair().getRaw(), targetNameX, 395);

                    if (script.getPlayerData().get(script.getTarget().getName()).get("combo_type") > -1)
                    {
                        g.drawImage(sidepanelCombo[script.getPlayerData().get(script.getTarget().getName()).get("combo_type")], 80, 405, null);
                    }

                    g.drawString(maxComboOutputString, 112, 419);
                    g.drawString(script.getPlayerData().get(script.getTarget().getName()).get("max_hit_melee") + "", 38, 467);
                    g.drawString(script.getPlayerData().get(script.getTarget().getName()).get("max_hit_ranged") + "", 74, 467);
                    g.drawString(script.getPlayerData().get(script.getTarget().getName()).get("max_hit_magic") + "", 111, 467);

                    if (Skills.getCurrentLevel(Skill.HITPOINTS) < script.getPlayerData().get(script.getTarget().getName()).get("eat_at"))
                    {
                        g.drawImage(redValueBar, 177, 357, null);
                    }
                }

                ///////////////////////////////////////////////////////

                g.setColor(COLOR_BLACK);

                if (healthStringX != 0)
                {
                    g.drawString(healthString, healthStringX, 369);
                }

                if (strengthBoostStringX != 0)
                {
                    g.drawString(strengthBoostString, strengthBoostStringX, 369);
                }

                if (myMaxComboOutputStringX != 0)
                {
                    g.drawString(myMaxComboOutputString, myMaxComboOutputStringX, 369);
                }

                if (ammoInQuiverStringX != 0)
                {
                    if (ammoInQuiver < 30)
                    {
                        g.drawImage(redValueBar, 472, 357, null);
                    }
                    g.drawString(ammoInQuiverString, ammoInQuiverStringX, 369);
                }

                ///////////////////////////////////////////////////////
            }
        }
        catch (Error | Exception e)
        {
            Log.severe(e);
        }
    }

    ///////////////////////////////////////////////////////////////////

    public Graphics getGraphics()
    {
        return graphics;
    }

    public String getHealthString()
    {
        return healthString;
    }

    public void setHealthString(String healthString)
    {
        this.healthString = healthString;
    }

    public void setHealthStringX(int healthStringX)
    {
        this.healthStringX = healthStringX;
    }

    public String getStrengthBoostString()
    {
        return strengthBoostString;
    }

    public void setStrengthBoostString(String strengthBoostString)
    {
        this.strengthBoostString = strengthBoostString;
    }

    public void setStrengthBoostStringX(int strengthBoostStringX)
    {
        this.strengthBoostStringX = strengthBoostStringX;
    }

    public String getAmmoInQuiverString()
    {
        return ammoInQuiverString;
    }

    public void setAmmoInQuiverString(String ammoInQuiverString)
    {
        this.ammoInQuiverString = ammoInQuiverString;
    }

    public void setAmmoInQuiverStringX(int ammoInQuiverStringX)
    {
        this.ammoInQuiverStringX = ammoInQuiverStringX;
    }

    public int getAmmoInQuiver()
    {
        return ammoInQuiver;
    }

    public void setAmmoInQuiver(int ammoInQuiver)
    {
        this.ammoInQuiver = ammoInQuiver;
    }

    public void setMaxComboOutputString(String maxComboOutputString)
    {
        this.maxComboOutputString = maxComboOutputString;
    }

    public void setTargetNameX(int targetNameX)
    {
        this.targetNameX = targetNameX;
    }

    public String getMyMaxComboOutputString()
    {
        return myMaxComboOutputString;
    }

    public void setMyMaxComboOutputString(String myMaxComboOutputString)
    {
        this.myMaxComboOutputString = myMaxComboOutputString;
    }

    public void setMyMaxComboOutputStringX(int myMaxComboOutputStringX)
    {
        this.myMaxComboOutputStringX = myMaxComboOutputStringX;
    }

    ///////////////////////////////////////////////////////////////////
}
