package com.zscorpio.pkhelper.helpers;

import com.zscorpio.pkhelper.PKHelper;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PlayerData
{
    ///////////////////////////////////////////////////////////////////

    private static final Map<String, Map<String, Integer>> PLAYER_DATA = new ConcurrentHashMap<>();

    ///////////////////////////////////////////////////////////////////

    public Map<String, Integer> get(String name)
    {
        if (PLAYER_DATA.containsKey(name))
        {
            return PLAYER_DATA.get(name);
        }
        else
        {
            PLAYER_DATA.put(name, new HashMap<String, Integer>()
            {
                {
                    put("max_hit_melee", 1);
                    put("max_hit_ranged", 1);
                    put("max_hit_magic", 1);
                    put("max_health", 10);
                    put("eat_at", 2);
                    put("combo_type", -1);
                }
            });

            PKHelper.getExecutor().execute(new HighscoresLookupRunnable(name));
            return PLAYER_DATA.get(name);
        }
    }

    ///////////////////////////////////////////////////////////////////

    public static Map<String, Map<String, Integer>> getPlayerData()
    {
        return PLAYER_DATA;
    }

    ///////////////////////////////////////////////////////////////////
}
