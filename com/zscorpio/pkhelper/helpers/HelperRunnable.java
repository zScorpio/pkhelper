package com.zscorpio.pkhelper.helpers;

import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.input.Keyboard;

import com.zscorpio.pkhelper.PKHelper;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.ui.Log;

import java.awt.event.KeyEvent;
import java.util.Arrays;

public class HelperRunnable implements Runnable
{
	///////////////////////////////////////////////////////////////////

	private final PKHelper script;

	///////////////////////////////////////////////////////////////////

	public HelperRunnable(PKHelper script)
	{

		this.script = script;
	}

	///////////////////////////////////////////////////////////////////

	@Override
	public void run()
	{
		while (!script.isStopping())
		{
			try
			{
				if (script.getKeyListener().getToggle() && !Bank.isOpen())
				{
					updatePaintHealth();
					updatePaintStrengthBoost();
					updatePaintQuiverArrows();
					updatePaintMaxComboOutput();
				}
				try
				{
					Thread.sleep(300);
				}
				catch (InterruptedException e)
				{
				}
			}
			catch (Exception | Error e)
			{
				Log.severe(e);
			}
		}
		Game.getCanvas().removeKeyListener(script.getKeyListener().getKeyAdapter());
		Game.getCanvas().removeMouseListener(script.getKeyListener().getMouseAdapter());
	}

	///////////////////////////////////////////////////////////////////

	private void updatePaintHealth()
	{
		if (script.getPaint().getGraphics() != null)
		{
			script.getPaint().setHealthString(Skills.getCurrentLevel(Skill.HITPOINTS)+"");
			script.getPaint().setHealthStringX(195 - (script.getPaint().getGraphics().getFontMetrics().stringWidth(script.getPaint().getHealthString()) / 2));
		}
	}

	///////////////////////////////////////////////////////////////////

	private void updatePaintStrengthBoost()
	{
		if (script.getPaint().getGraphics() != null)
		{
			script.getPaint().setStrengthBoostString((Skills.getCurrentLevel(Skill.STRENGTH) - Skills.getLevel(Skill.STRENGTH)) + "");

			if (Skills.getCurrentLevel(Skill.STRENGTH) > Skills.getLevel(Skill.STRENGTH))
			{
				script.getPaint().setStrengthBoostString("+" + script.getPaint().getStrengthBoostString());
			}
			else if (Skills.getCurrentLevel(Skill.STRENGTH) < Skills.getLevel(Skill.STRENGTH))
			{
				script.getPaint().setStrengthBoostString("-" + script.getPaint().getStrengthBoostString());
			}

			script.getPaint().setStrengthBoostStringX(292 - (script.getPaint().getGraphics().getFontMetrics().stringWidth(script.getPaint().getStrengthBoostString()) / 2));
		}
	}

	///////////////////////////////////////////////////////////////////

	private void updatePaintQuiverArrows()
	{
		Keyboard.pressEventKey(KeyEvent.VK_F4);
		Time.sleep(30);
		if (script.getPaint().getGraphics() != null)
		{
			if (Interfaces.getComponent(387, 16, 1) != null)
			{
				script.getPaint().setAmmoInQuiver(Interfaces.getComponent(387, 16, 1).getItemStackSize());
				script.getPaint().setAmmoInQuiverString(script.getPaint().getAmmoInQuiver() + "");
				script.getPaint().setAmmoInQuiverStringX(490 - (script.getPaint().getGraphics().getFontMetrics().stringWidth(script.getPaint().getAmmoInQuiverString()) / 2));
			}
		}
		Keyboard.pressEventKey(KeyEvent.VK_ESCAPE);
	}

	///////////////////////////////////////////////////////////////////

	private void updatePaintMaxComboOutput()
	{
		if (script.getPaint().getGraphics() != null)
		{
			int maxHitMelee = getMeleeMaxHit(Skills.getCurrentLevel(Skill.STRENGTH), Skills.getLevel(Skill.PRAYER));
			int maxHitRanged = getRangedMaxHit(Skills.getLevel(Skill.RANGED), Skills.getLevel(Skill.PRAYER));

			script.getPaint().setMyMaxComboOutputString((maxHitRanged + maxHitMelee)+"");
			script.getPaint().setMyMaxComboOutputStringX(400 - (script.getPaint().getGraphics().getFontMetrics().stringWidth(script.getPaint().getMyMaxComboOutputString()) / 2));

			if (script.getTarget() != null)
			{
				script.getPaint().setMaxComboOutputString(script.getPlayerData().get(script.getTarget().getName()).get("eat_at") + "");
				script.getPaint().setTargetNameX(72 - (script.getPaint().getGraphics().getFontMetrics().stringWidth(script.getTarget().getNamePair().getRaw()) / 2));
			}
		}
	}

	///////////////////////////////////////////////////////////////////

	private int getMeleeMaxHit(int strengthLevel, int prayerLevel)
	{
		int styleBonus = 3;
		double prayerBonus = 1;
		if (prayerLevel >= 31)
			prayerBonus = 1.15;
		else if (prayerLevel >= 13)
			prayerBonus = 1.1;
		else if (prayerLevel >= 4)
			prayerBonus = 1.05;

		// trunc( ( Str Level + Pot Bonus ) X Prayer Bonus X Other Bonus ) + Style Bonus
		double effectiveStrength = (int) (((strengthLevel) * prayerBonus) + styleBonus);
		// 80 refers to Rune 2h sword (70) + Amulet of strength (10) bonuses
		double meleeStrengthBonus = 80.0;
		double baseDamage = 1.3 + (effectiveStrength / 10.0) + (meleeStrengthBonus / 80.0) + ((effectiveStrength * meleeStrengthBonus) / 640.0);
		return (int) baseDamage;
	}

	///////////////////////////////////////////////////////////////////

	private int getRangedMaxHit(int rangedLevel, int prayerLevel)
	{
		double prayerBonus = 1;
		prayerBonus = 1;
		if (prayerLevel >= 44)
			prayerBonus = 1.15;
		else if (prayerLevel >= 26)
			prayerBonus = 1.1;
		else if (prayerLevel >= 8)
			prayerBonus = 1.05;

		// trunc( ( Rng Level + Pot Bonus ) X Prayer Bonus X Other Bonus) ) + Style Bonus
		double effectiveStrength = (int) (((rangedLevel) * prayerBonus));
		// 31 refers to Adamant arrow (30) bonus
		double rangedStrengthBonus = 31.0;
		double baseDamage = 1.3 + (effectiveStrength / 10.0) + (rangedStrengthBonus / 80.0) + ((effectiveStrength * rangedStrengthBonus) / 640.0);
		return (int) baseDamage;
	}

	///////////////////////////////////////////////////////////////////
}
