package com.zscorpio.pkhelper.helpers;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.GrandExchange;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.input.Keyboard;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.ui.Log;

import com.zscorpio.pkhelper.PKHelper;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class KeyListener
{
	///////////////////////////////////////////////////////////////////

	public static final int KEY_KO_2H = KeyEvent.VK_Q;
	public static final int KEY_RANGED = KeyEvent.VK_W;
	public static final int KEY_EAT = KeyEvent.VK_E;
	public static final int KEY_DRINK_POTION = KeyEvent.VK_R;
	public static final int KEY_LOOT_ARROWS = KeyEvent.VK_F;
	private static final int KEY_TOGGLE = KeyEvent.VK_T;

	////////////////////////////////////////////////////////////////////

	private final PKHelper script;
	private boolean toggle = true;
	private boolean performing = false;
	private int performingKey = -1;
	private boolean performingKeyReleased = true;

	private boolean toToggle = false;
	private final String[] food = {"Swordfish", "Lobster"};
	private final String[] potions = {"Strength potion(1)", "Strength potion(2)", "Strength potion(3)", "Strength potion(4)"};

	private static final List<String> KO_2H_ITEMS = new ArrayList<String>()
	{
		{
			addAll(Arrays.asList("Rune 2h sword", "Amulet of strength"));
		}
	};

	private static final List<String> RANGED_ITEMS = new ArrayList<String>()
	{
		{
			addAll(Arrays.asList("Maple shortbow", "Amulet of power", "Adamant arrow"));
		}
	};

	private final MouseAdapter mouseAdapter = new MouseAdapter()
	{
		@Override
		public void mousePressed(MouseEvent e)
		{
			if (e.getX() >= 1 && e.getY() >= 339 && e.getX() <= 142 && e.getY() <= 386 && !GrandExchange.isOpen())
			{
				toggle = !toggle;
			}
		}
	};

	private final KeyAdapter keyAdapter = new KeyAdapter()
	{
		@Override
		public void keyTyped(KeyEvent e)
		{
			if ((toggle || toToggle) && Game.isLoggedIn())
			{
				if (toToggle)
				{
					toggle = !toggle;
					toToggle = false;
				}
				Keyboard.pressEventKey(KeyEvent.VK_BACK_SPACE);
			}
		}

		@Override
		public void keyReleased(KeyEvent e)
		{
			if (performingKey == e.getKeyCode())
			{
				performingKeyReleased = true;
				performingKey = -1;
			}
		}

		@Override
		public void keyPressed(KeyEvent e)
		{
			if (toggle && Game.isLoggedIn())
			{
				if (e.getKeyCode() == KEY_TOGGLE)
				{
					toToggle = true;
				}
				if (toggle && !performing)
				{
					switch (e.getKeyCode())
					{
						case KEY_EAT:
						{
							eatfood();
							break;
						}
						case KEY_KO_2H:
						{
							ko2H();
							break;
						}
						case KEY_RANGED:
						{
							ranged();
							break;
						}
						case KEY_DRINK_POTION:
						{
							drinkPotions();
							break;
						}
						case KEY_LOOT_ARROWS:
						{
							lootArrows();
							break;
						}
						case KeyEvent.VK_SPACE:
						{
							if(script.getTarget() != null)
							{
								script.getTarget().interact("Attack");
							}
							break;
						}
					}
				}
			}
		}
	};

	///////////////////////////////////////////////////////////////////

	public KeyListener(PKHelper script)
	{
		this.script = script;
		Game.getCanvas().addKeyListener(keyAdapter);
		Game.getCanvas().addMouseListener(mouseAdapter);
	}

	///////////////////////////////////////////////////////////////////

	private void lootArrows()
	{
		setPerformingKey(KEY_LOOT_ARROWS);
		Pickable item = Pickables.getBest(zComparator, pickable -> pickable.getPosition().getY() >= 3523 && pickable.getPosition().distance() < 6 && pickable.getName().equals("Adamant arrow"));
		if (item != null)
		{
			item.interact("Take");
			Log.fine("[zArrowLooter]--> Looting " + item.getStackSize());
		}
		performing = false;
	}

	private final Comparator<? super Pickable> zComparator = (Comparator<Pickable>) (firstItem, secondItem) ->
	{
		if (firstItem.getStackSize() > secondItem.getStackSize())
		{
			return -1;
		}
		else if (firstItem.getStackSize() == secondItem.getStackSize())
		{
			return Double.compare(secondItem.getPosition().distance(), firstItem.getPosition().distance()) * -1;
		}
		else
		{
			return 1;
		}
	};

	///////////////////////////////////////////////////////////////////

	private void drinkPotions()
	{
		setPerformingKey(KEY_DRINK_POTION);
		if (Inventory.contains(potions))
		{
			Inventory.getFirst(potions).interact("Drink");
		}
		if(script.getTarget() != null)
		{
			script.getTarget().interact("Attack");
		}
		performing = false;
	}

	///////////////////////////////////////////////////////////////////

	private void ranged()
	{
		setPerformingKey(KEY_RANGED);
		Item[] items = Inventory.getItems(item -> RANGED_ITEMS.contains(item.getName()));
		for (Item item : items)
		{
			if (item.getName().contains("Amulet"))
			{
				item.interact("Wear");
			}
			else
			{
				item.interact("Wield");
			}
			if (Inventory.contains(RANGED_ITEMS.toArray(new String[0])))
				Time.sleep(90);
		}
		if(script.getTarget() != null)
		{
			script.getTarget().interact("Attack");
		}
		performing = false;
	}

	///////////////////////////////////////////////////////////////////

	private void ko2H()
	{
		setPerformingKey(KEY_KO_2H);
		if(script.getTarget() != null)
		{
			Movement.walkTo(script.getTarget());
		}
		Item[] items = Inventory.getItems(item -> KO_2H_ITEMS.contains(item.getName()));
		for (Item item : items)
		{
			if (item.getName().contains("Amulet"))
			{
				item.interact("Wear");
			}
			else
			{
				item.interact("Wield");
			}
			if (Inventory.contains(KO_2H_ITEMS.toArray(new String[0])))
				Time.sleep(90);
		}
		if(script.getTarget() != null)
		{
			script.getTarget().interact("Attack");
		}
		performing = false;
	}

	///////////////////////////////////////////////////////////////////

	private void eatfood()
	{
		setPerformingKey(KEY_EAT);
		if (Inventory.contains(food))
		{
			Inventory.getFirst(food).interact("Eat");
		}
		if(script.getTarget() != null)
		{
			script.getTarget().interact("Attack");
		}
		performing = false;
	}

	///////////////////////////////////////////////////////////////////

	private void setPerformingKey(int key)
	{
		performing = true;
		performingKeyReleased = false;
		performingKey = key;
	}

	///////////////////////////////////////////////////////////////////

	public KeyAdapter getKeyAdapter()
	{
		return keyAdapter;
	}

	public MouseAdapter getMouseAdapter()
	{
		return mouseAdapter;
	}

	public boolean isPerforming()
	{
		return performing;
	}

	public boolean getToggle()
	{
		return toggle;
	}

	public int getPerformingKey()
	{
		return performingKey;
	}

	public boolean isPerformingKeyReleased()
	{
		return performingKeyReleased;
	}

	///////////////////////////////////////////////////////////////////
}
