package com.zscorpio.pkhelper.helpers;

import org.rspeer.ui.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class HighscoresLookupRunnable implements Runnable
{
    ///////////////////////////////////////////////////////////////////

    private String name;
    private static final int MAX_HIT_RANGED_MELEE = 0;
    private static final int MAX_HIT_RANGED_MAGIC = 1;
    private static final int MAX_HIT_MELEE_MAGIC = 2;
    private static final int MAX_HIT_MAGIC = 3;

    ///////////////////////////////////////////////////////////////////

    public HighscoresLookupRunnable(String name)
    {
        this.name = name;
    }

    ///////////////////////////////////////////////////////////////////

    @Override
    public void run()
    {
        try
        {
            URL obj = new URL("https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player=" + name);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                String inputLine;
                StringBuilder response = new StringBuilder();

                try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream())))
                {
                    while ((inputLine = in.readLine()) != null)
                    {
                        response.append(inputLine).append(",");
                    }
                }

                String[] token = response.toString().split(",");
                int strengthLevel = Integer.parseInt(token[10]);
                int hitpointsLevel = Integer.parseInt(token[13]);
                int rangedLevel = Integer.parseInt(token[16]);
                int prayerLevel = Integer.parseInt(token[19]);
                int magicLevel = Integer.parseInt(token[21]);

                int maxHitMelee = getMeleeMaxHit(strengthLevel, prayerLevel);
                int maxHitRanged = getRangedMaxHit(rangedLevel, prayerLevel);
                int maxHitMagic = getMagicMaxHit(magicLevel);

                int eatAt, maxHitType;
                if (rangedLevel >= 30)
                {
                    if (maxHitMelee > 1 && maxHitMelee > maxHitMagic)
                    {
                        maxHitType = MAX_HIT_RANGED_MELEE;
                        eatAt = maxHitRanged + maxHitMelee + 1;
                    }
                    else
                    {
                        maxHitType = MAX_HIT_RANGED_MAGIC;
                        eatAt = maxHitRanged + maxHitMagic + 1;
                    }
                }
                else if (strengthLevel > 1)
                {
                    maxHitType = MAX_HIT_MELEE_MAGIC;
                    eatAt = maxHitMelee + maxHitMagic + 1;
                }
                else
                {
                    maxHitType = this.MAX_HIT_MAGIC;
                    eatAt = maxHitMagic;
                }

                PlayerData.getPlayerData().put(name, new HashMap<String, Integer>()
                {
                    {
                        put("max_hit_melee", maxHitMelee);
                        put("max_hit_ranged", maxHitRanged);
                        put("max_hit_magic", maxHitMagic);
                        put("max_health", hitpointsLevel);
                        put("eat_at", eatAt);
                        put("combo_type", maxHitType);
                    }
                });
            }
            else
            {
                Log.fine("Bad HTTP Response: " + con.getResponseCode());
            }
        }
        catch (Exception e)
        {
            Log.severe(e);
        }
    }

    ///////////////////////////////////////////////////////////////////

    private int getMeleeMaxHit(int strengthLevel, int prayerLevel)
    {
        int styleBonus = 3;
        double potionBonus = (10.0 / 100.0 * (double) strengthLevel) + 3.0;
        double prayerBonus = 1;
        if (prayerLevel >= 31)
            prayerBonus = 1.15;
        else if (prayerLevel >= 13)
            prayerBonus = 1.1;
        else if (prayerLevel >= 4)
            prayerBonus = 1.05;

        // trunc( ( Str Level + Pot Bonus ) X Prayer Bonus X Other Bonus ) + Style Bonus
        double effectiveStrength = (int) (((strengthLevel + potionBonus) * prayerBonus) + styleBonus);
        // 80 refers to Rune 2h sword (70) + Amulet of strength (10) bonuses
        double meleeStrengthBonus = 80.0;
        double baseDamage = 1.3 + (effectiveStrength / 10.0) + (meleeStrengthBonus / 80.0) + ((effectiveStrength * meleeStrengthBonus) / 640.0);
        return (int) baseDamage;
    }

    ///////////////////////////////////////////////////////////////////

    private int getRangedMaxHit(int rangedLevel, int prayerLevel)
    {
        double prayerBonus = 1;
        prayerBonus = 1;
        if (prayerLevel >= 44)
            prayerBonus = 1.15;
        else if (prayerLevel >= 26)
            prayerBonus = 1.1;
        else if (prayerLevel >= 8)
            prayerBonus = 1.05;

        // trunc( ( Rng Level + Pot Bonus ) X Prayer Bonus X Other Bonus) ) + Style Bonus
        double effectiveStrength = (int) (((rangedLevel) * prayerBonus));
        // 31 refers to Adamant arrow (30) bonus
        double rangedStrengthBonus = 31.0;
        double baseDamage = 1.3 + (effectiveStrength / 10.0) + (rangedStrengthBonus / 80.0) + ((effectiveStrength * rangedStrengthBonus) / 640.0);
        return (int) baseDamage;
    }

    ///////////////////////////////////////////////////////////////////

    private int getMagicMaxHit(int magicLevel)
    {
        int maxHitMagic;
        if (magicLevel >= 59)
            maxHitMagic = 16;
        else if (magicLevel >= 53)
            maxHitMagic = 15;
        else if (magicLevel >= 47)
            maxHitMagic = 14;
        else if (magicLevel >= 41)
            maxHitMagic = 13;
        else if (magicLevel >= 35)
            maxHitMagic = 12;
        else if (magicLevel >= 29)
            maxHitMagic = 11;
        else if (magicLevel >= 23)
            maxHitMagic = 10;
        else if (magicLevel >= 17)
            maxHitMagic = 9;
        else if (magicLevel >= 13)
            maxHitMagic = 8;
        else if (magicLevel >= 9)
            maxHitMagic = 6;
        else if (magicLevel >= 5)
            maxHitMagic = 4;
        else
            maxHitMagic = 2;
        return maxHitMagic;
    }

    ///////////////////////////////////////////////////////////////////
}
