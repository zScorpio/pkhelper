package com.zscorpio.pkhelper;

import org.rspeer.runetek.adapter.component.InterfaceComponent;
import org.rspeer.runetek.adapter.scene.PathingEntity;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.runetek.providers.RSPlayer;
import org.rspeer.runetek.providers.subclass.GameCanvas;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import com.zscorpio.pkhelper.helpers.HelperRunnable;
import com.zscorpio.pkhelper.helpers.KeyListener;
import com.zscorpio.pkhelper.helpers.Paint;
import com.zscorpio.pkhelper.helpers.PlayerData;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@ScriptMeta(name = "PKHelper", developer = "zScorpio", desc = "An F2P PK helping tool", version = 0.02)
public class PKHelper extends Script implements RenderListener
{
    ///////////////////////////////////////////////////////////////////

    private Paint paint;
    private KeyListener keyListener;
    private PlayerData playerData;
    private Player target;
    private PathingEntity<RSPlayer, Player> bitch;
    private Deque<Player> possibleTargets;
    private List<Pickable> availablePickables;

    ///////////////////////////////////////////////////////////////////

    private String status = "";
    private long targetClearTimer;
    private Player localPlayer;
    private InterfaceComponent wildernessLevelComponent;
    private static ExecutorService executor;

    ///////////////////////////////////////////////////////////////////

    @Override
    public void onStart()
    {
        if (Game.isLoggedIn())
        {
            possibleTargets = new ConcurrentLinkedDeque<>();
            availablePickables = new ArrayList<>();
            executor = Executors.newFixedThreadPool(2);
            localPlayer = Players.getLocal();
            paint = new Paint(this);
            keyListener = new KeyListener(this);
            playerData = new PlayerData();
            executor.execute(new HelperRunnable(this));
            setStatus("Started PKHelper - Made by zScorpio");
            GameCanvas.setInputEnabled(true);
        }
        else
        {
            setStopping(true);
        }
    }

    @Override
    public void onStop()
    {
        Game.getCanvas().removeKeyListener(keyListener.getKeyAdapter());
        Game.getCanvas().removeMouseListener(keyListener.getMouseAdapter());
        executor.shutdown();
        try
        {
            if (!executor.awaitTermination(1000, TimeUnit.MILLISECONDS))
            {
                executor.shutdownNow();
            }
        }
        catch (InterruptedException e)
        {
            executor.shutdownNow();
        }
        setStatus("Stopped PKHelper - Made by zScorpio");
    }

    ///////////////////////////////////////////////////////////////////

    @Override
    public void notify(RenderEvent renderEvent)
    {
        paint.draw(renderEvent.getSource(), this);
    }

    ///////////////////////////////////////////////////////////////////

    @Override
    public int loop()
    {
        try
        {
            if (keyListener.getToggle())
            {
                availablePickables = Arrays.asList(Pickables.getLoaded(item -> item.getName().equals("Adamant arrow")));
                if (localPlayer.getTarget() == null || !localPlayer.getTarget().getClass().getSimpleName().equals("Player"))
                {
                    if (target != null)
                    {
                        if (targetClearTimer == 0)
                        {
                            targetClearTimer = System.currentTimeMillis();
                        }
                        else if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - targetClearTimer) >= 6)
                        {
                            target = null;
                            getPaint().setTargetNameX(0);
                            targetClearTimer = 0;
                        }
                    }
                    possibleTargets.clear();
                    if ((wildernessLevelComponent = Interfaces.getFirst(90, comp -> comp.getText().startsWith("Level:"))) != null)
                    {
                        String[] tokens = wildernessLevelComponent.getText().split(" ");
                        if (tokens.length > 1)
                        {
                            int wildernessLevel = Integer.parseInt(tokens[1].trim());
                            possibleTargets.addAll(Arrays.asList(Players.getLoaded(player -> !player.equals(localPlayer) && Math.abs(localPlayer.getCombatLevel() - player.getCombatLevel()) <= wildernessLevel)));
                        }
                    }
                }
                else
                {
                    Player newTarget = (Player) localPlayer.getTarget();
                    if (target != null && !target.equals(newTarget))
                    {
                        targetClearTimer = 0;
                        paint.setTargetNameX(0);
                        target = newTarget;
                    }
                    else if (target == null)
                    {
                        targetClearTimer = 0;
                        target = newTarget;
                    }
                    PathingEntity<RSPlayer, Player> playerAttackingUs = Players.getNearest(player -> player.getTarget() != null && !player.equals(target) && player.getTarget().equals(localPlayer));

                    if (playerAttackingUs != null)
                    {
                        bitch = playerAttackingUs;
                    }
                }
            }
        }
        catch (Error | Exception e)
        {
            Log.severe(e);
        }
        return 300;
    }

    ///////////////////////////////////////////////////////////////////

    private void setStatus(String status)
    {
        if (!this.status.equals(status))
        {
            this.status = status;
            Log.fine(status);
        }
    }

    public static ExecutorService getExecutor()
    {
        return executor;
    }

    public List<Pickable> getAvailablePickables()
    {
        return availablePickables;
    }

    public Player getTarget()
    {
        return target;
    }

    public PathingEntity<RSPlayer, Player> getBitch()
    {
        return bitch;
    }

    public KeyListener getKeyListener()
    {
        return keyListener;
    }

    public Paint getPaint()
    {
        return paint;
    }

    public PlayerData getPlayerData()
    {
        return playerData;
    }

    public Deque<Player> getPossibleTargets()
    {
        return possibleTargets;
    }

    ///////////////////////////////////////////////////////////////////
}
